/*------------------------------------------------------
My name: Brenna Bensley
My student number: 5815551
My email address: brb405@uowmail.edu.au
Assignment number: 1
-------------------------------------------------------*/ 
import java.util.Scanner; 
import java.lang.Math;

class BlastSafety {
	public static void main(String[] args) {
		//Load in Scanner
		Scanner scan = new Scanner(System.in);
		//Create New line variable
		String newLine = System.lineSeparator(); 

		//Constants
	//-----------------------------------------------------

		//Factor Of Safety
		final double FOS = 2.0;

		//Explosive Density (kg/cm^3)
		final double ED =  1.2; 


		//Input Variables
	//-----------------------------------------------------

		//Start collecting input
		System.out.println("- Blast safe distance estimation - " + newLine);
		System.out.println("Enter the blast parameters ");

		//Get user input for Charge Length
		System.out.print("Charge Length (m): ");
		double chargeLength = scan.nextDouble();
				

		//Get user input for Stemming Length
		System.out.print("Stemming Length (m): ");
		double stemmingLength = scan.nextDouble();


		//Get user input for Borehole Diamater
		System.out.print("Borehole Diameter (mm): ");
		double boreholeDiameter = scan.nextDouble();



		//Formula
	//-----------------------------------------------------

		//Contributing Charge Length Factor
		//cclf = 1000 × cl / d
		double cclf = 1000 * chargeLength / boreholeDiameter;

		//If cclf is greater than 10, limit it to 10.
		if (cclf > 10) {
			
			cclf = 10;
			
		}
		
		//Forumla for Depth of Burial, symbolised as 'sdob' in the original forumla.
		//sdob = ( sl + 0.0005 × cclf × d ) / ( 0.00923 × (cclf × d^3 × ED)^0.333 )
		double depthOfBurial = (stemmingLength + 0.0005 * cclf * boreholeDiameter ) 
			/ 
			(0.00923 * Math.pow( //raise (cclf × d3× ED) to the power of 0.333
				(cclf *  Math.pow(//raise boreholeDiameter to the power of 3
					boreholeDiameter, 3)* ED),
			0.333));

				


		//sdob = FOS × 11 × sdob^-2.167 × d^0.667
		double safeDistanceDouble = FOS * 11 * Math.pow(depthOfBurial,-2.167) * Math.pow(boreholeDiameter, 0.667);

		//Convert safeDistanceDouble to an integer.
		int safeDistanceInt = (int) safeDistanceDouble;

		//Output
	//-------------------------------------------------------	
		
		//display results. 	
		System.out.print(newLine);
		System.out.println("The blast safe distance is " + safeDistanceInt + " meters.");
	}

}