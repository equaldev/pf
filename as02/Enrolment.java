/*------------------------------------------------------
My name: Brenna Bensley
My student number: 5815551
My email address: brb405@uowmail.edu.au
Assignment number: 2
-------------------------------------------------------*/ 

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

class Enrolment {

    public static void main(String[] args) throws FileNotFoundException{
     
//Student Details
Scanner studentStream = new Scanner(new File(args[0]));

        // First Name
        String tmp = studentStream.next(); // skip first string
        tmp = studentStream.next(); // skip second
        String firstName = studentStream.next(); // save third string

        //Last name
        tmp = studentStream.next(); // skip first string
        tmp = studentStream.next(); // skip second
        String lastName = studentStream.next(); // save third
        //Course
        tmp = studentStream.next(); // skip first string
        String course = studentStream.next(); // save course string

        //Student Number
        tmp = studentStream.next(); // skip first string
        tmp = studentStream.next(); // skip second string
        int sNumber = studentStream.nextInt(); // save third
        
        //Account for email
        tmp = studentStream.next(); // skip first string
        String acc = studentStream.next(); // save second

        //studentStream.close();

        //Concatenate First and Last name
        String fullName = firstName + " " + lastName;

        Student stud1 = new Student(); // create student object

//Subject Details 
Scanner subjectStream = new Scanner(new File(args[1]));

        // course code
        tmp = subjectStream.next(); // skip first string
        tmp = subjectStream.next(); // skip second
        String courseCode = subjectStream.next(); // save third string

        //Session
        tmp = subjectStream.next(); // skip first
        String session = subjectStream.next(); // save second

        //Year
        tmp = subjectStream.next(); // skip first
        int year = subjectStream.nextInt(); // save second

        Subject sub1 = new Subject(); // create subject object

        //set student details
        stud1.setName( fullName );
        stud1.setCourse( course ); 
        stud1.setSNo( sNumber ); 
        stud1.setEmail( acc ); 

        //set subject details
        sub1.setCode( courseCode ); 
        sub1.setSession( session ); 
        sub1.setYear( year ); 

        //Output report
        stud1.displayStudInfo();
        sub1.displaySubjectInfo();

    }

}

class Student {
    private String fullName;
    private int stdNumber;
    private String email;
    private String course;

    public Student( String name ) {
  

    } 

    public Student() {
 
    }

     public Student(Student student) {
 
    }

    public void setEmail(String acc) {
        email = acc + "@uow.edu.au";
    }  

    public void setName(String name) {
        fullName = name;
    }

     public void setCourse(String courseCode) {
        course = courseCode;
    }
    public void setSNo(int sNo) {
        stdNumber = sNo;
    }
    public void displayStudInfo() {
        System.out.println("STUDENT");
        System.out.println("Full Name: " + fullName);
        System.out.println("Course: " + course);
        System.out.println("Student Number: " + stdNumber);
        System.out.println("Email: " + email);

    }
}


class Subject {
    private String sCode;
    private String session;
    private int year;

    public Subject( String name ) {
 
    } 
    public Subject() {
 
    }
     public Subject(Subject subject) {
 
    }
    public void setSession(String tmpSession){
        session = tmpSession;
    }
    public void setCode(String code) {
        sCode = code;  
    }  
    public void setYear(int tmpYear) {
        year = tmpYear;  
    }  

    public void displaySubjectInfo() {

                System.out.println("ENROLMENT");
                System.out.println("Subject Code: " + sCode);
                System.out.println("Session: " + session);
                System.out.println("Year: " + year);

    }
}