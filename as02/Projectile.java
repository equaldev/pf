import java.util.Scanner;
import java.lang.Math;

class Projectile {

    private static final double G = 9.8;
    private static double maxHeight;
    public static double initalVelocity;
    public static double hMax;

    public static void main(String[] args) {
        System.out.println("Enter an initial velocity:");

        Scanner keyboard = new Scanner(System.in);

        double initialVelocity = keyboard.nextDouble();

        maxHeight = calculateMaxHeight(initialVelocity);

        System.out.println(maxHeight);

    }
//hMax = vo2/(2*G) 
    public static double calculateMaxHeight(double args) {
        
       

        //Calculation
        double hMax = Math.pow(args, 2)/(2*G);
        return hMax;
    }
}