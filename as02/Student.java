import java.util.Scanner;

class StudentAccount {

    private String sName;
    public String getName() { return sName; }
    public void setName(String name) { sName = name; }
}

class AccountSystem {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner( System.in );
        System.out.print(" Enter a name: ");
        String aName = keyboard.next(); // read a word
        StudentAccount stud1 = new StudentAccount(); // create an object
        stud1.setName( aName ); // call a public method to set sName
        String name1 = stud1.getName(); // get a private field
        System.out.println("Student's name: " + name1);
    }
}