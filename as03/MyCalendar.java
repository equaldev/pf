
/*------------------------------------------------------
My name: Brenna Bensley
My student number: 5815551
My email address: brb405@uowmail.edu.au
Assignment number: 3
-------------------------------------------------------*/ 

import java.util.Scanner;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;
import java.util.GregorianCalendar;
import java.util.ArrayList;
import java.util.Calendar;




class Day {
    public Day() {
                
    }


}
class MyCalendar {

    private MyDate myDate;
    private static String day;
    public static ArrayList<String> days  = new ArrayList<String>(6);
    public static ArrayList<String> months  = new ArrayList<String>(14);

    public static String monthName;
    public static int monthIndex;
 public static void main(String[] args) throws ParseException{
     //fill array of day names
       days.add("Saturday");
        days.add("Sunday");
        days.add("Monday");
        days.add("Tuesday");
        days.add("Wednesday");
        days.add("Thursday");
        days.add("Friday");       
        //fill array of month names
        months.add("empty");
        months.add("empty");
        months.add("March");
        months.add("April");
        months.add("May");
        months.add("June");
        months.add("July");
        months.add("August");
        months.add("September");
        months.add("October");
        months.add("November");
        months.add("December");
        months.add("January");
        months.add("Februrary");
       
        Scanner input = new Scanner(System.in);
        //Get user input
        System.out.println("Enter a date: ");
        String dateString = input.next();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = formatter.parse(dateString);

        //Perform conversions on entered date
            int month = month(date);
            int day = dayMonth(date);
            int dayWeek = dayWeek(date);
            int year = year(date);
            int indexYear = indexYear(date);
            MyDate enteredDate  = new MyDate(day, month, year);


        /*
        Calculations
        -------------------------------------------------------------
       where
        h is the day of the week (0 = Saturday, 1 = Sunday, 2 = Monday, 3=Tuesday, 4=Wednesday,
        5=Thursday, 6 = Friday)*/
        int h = dayWeek;
       
       /*q is the day of the month*/
       int q = day;
       /*
        m is the month (3 = March, 4 = April, 5 = May, 6=June, 7=July, 8=August, 9=September,
        10=October, 11=November, 12=December, 13=January, 14 = February)
        */
        int m = month;
        /*
        K is the year of the century (year mod 100).
        */
        int k = year(date);

        /*J is the zero-based century (year/100).*/
        int j = indexYear(date);
        /*
        Formula
        -----------------
        */
        h = (q + ((13 * (m + 1 ))/5) + k + (k / 4) + (j / 4 ) + (5 * j)) % 7;


        int weekOfMonth = weekOfMonth(date);
        monthIndex = enteredDate.getMonth() - 1;
        monthName = months.get(monthIndex);

        //Output
        System.out.println(); 
        System.out.println(dateString + " is a "+ getDayName(dayWeek) + " and locates in week " + weekOfMonth(date) + " of " + monthName); 
        printCalendar();

    }
    public MyCalendar(MyDate date) {
        monthName = months.get(monthIndex);

    }
    public static String getDayName(int dayindex){
       day = days.get(dayindex);
       
       return day;
    }
    public static int weekOfMonth(Date aDate){
        Calendar cal = Calendar.getInstance();
        cal.setTime(aDate);
        return cal.get(Calendar.WEEK_OF_MONTH);
    }
    public static int dayWeek(Date aDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(aDate);
        return cal.get(Calendar.DAY_OF_WEEK);
    }
    public static int dayMonth(Date aDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(aDate);
        return cal.get(Calendar.DAY_OF_MONTH);
    }
    public static int month(Date aDate) {
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(aDate);
        int modMonth = cal.get(GregorianCalendar.MONTH);

        if(modMonth >= 2 && modMonth <= 11) {

            modMonth = modMonth + 1;

        } else if (modMonth == 0){
            modMonth = modMonth + 13;

        } else if (modMonth == 1){
            modMonth = modMonth + 13;

        }
        return modMonth;

    }
    //Get the year Index
    public static int indexYear(Date aDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(aDate);

        int x = cal.get(Calendar.YEAR);

        String x_str = Integer.toString(x);

        int yearIndex = Integer.parseInt(Integer.toString(x).substring(0, 2));         

        return yearIndex;
    }
    //Get the year mod value
    public static int year(Date aDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(aDate);

        int x = cal.get(Calendar.YEAR);

        int yearMod = x % 100;

        return yearMod;
    }
    public static void printCalendar() {
        int cols = 7;
        int offset = 0; // represent days 0-6
        int day = 1 - offset;
        int endDay;
        if(monthName == "April" ||
            monthName == "June" ||
            monthName == "September" ||
            monthName == "November") {

            endDay = 30;

        } else if (monthName == "Februrary") {
                endDay = 28;
        } else {
            endDay = 31;

        }
        String days = " Mon  Tue  Wed  Thu  Fri  Sat  Sun";

                System.out.println(); 
 
                System.out.println("Calendar for " + monthName +":");
                System.out.println(); 

                System.out.println(days);

        for (int j = day; j <= endDay; j++) {
 
            if (j > 0) {

                if (j < 10 && offset > cols) { 
                 System.out.printf("%4d ", j);
            } else { 
                 System.out.printf("%4d ", j);
            }
            } else {
                System.out.printf("%4"); 
            }
            
            if (j > 0 && j % cols==0) {
                System.out.println(); // new row
            } 
            
            day = j + 1;
            
            } 

                System.out.println();
                }
   

}

class MyDate {
    private int day;
    private int month;
    private int year;
    private Boolean valid;

    public MyDate( int day, int month, int year) {

        if (isDateValid(day, month)) {
            this.day = day;
            this.month = month;
            this.year = year;
            this.valid = true;
        } else {
            System.out.println("Invalid");
        }
    }

    public int getDay(){
        return this.day;
    };
     public int getMonth(){
        return this.month;
    };
     public int getYear(){
        return this.year;
    };

    public Boolean isDateValid(int day, int month){
        if (day <= 31) {
            return true;
        }
        else {
            return false;
        }
    };



}
