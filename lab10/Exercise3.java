import java.lang.Math;

class computePI {
	public static void main(String[] args) {


		int d = 1;

		double sum = 0;
		
        for(; d<100; d=d+2) {

    		if (d % 4 == 1) {
			    sum += 1/d;
            }
	
        	else if (d % 4 == 3) {
        		sum -= 1/d;
            }

	    	else {
			    System.out.println("Oops.");            
             } 
		}

		sum = sum * 4;


		System.out.println("Difference between Math.PI and calculated value of PI = " + (Math.PI / sum*100) + "%");


	}

}