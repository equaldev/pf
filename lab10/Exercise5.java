  import java.util.Scanner;  
  
  class TimeTable {
	public static void main(String[] args) {
        
        
    Scanner input = new Scanner(System.in); 

    System.out.print("Enter an integer: ");

    int n = input.nextInt();

        for(int i = 1; i <= n; i++) {
            //if i is one, print the heading line
            if (i == 1) {
                System.out.println("---------------------------------------------------------------");
            }
        
            //otherwise, print multiplication for each value
            for (int j = 1; j <= n; j++) {
                System.out.printf("%1$d\t", i * j);
            }
            //then go to the next line
        System.out.println(); 
        }

     }
}