import java.util.Scanner;

import java.lang.Math;

class SumAndAverage {

    private static int n; 

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter a postive number:");
        n = input.nextInt();

        if (n < 1) {
            System.out.println("Please enter a postive number:");
            n = input.nextInt();  
        }

        int sum = 0; 
        int i = 0; 
        int count = 0;  

        while (i <= n) {
            sum += i; 
            i++;
            count++;
        }

        System.out.println("The sum of 1 to " + n + " is " + sum);
        System.out.println("The average of the sum is " + sum/count);
    }  
}