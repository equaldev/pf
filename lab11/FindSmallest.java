import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
class FindSmallest {
    

    public static void main (String[] args) {
        Integer[] array;
        int min = 1;
        int max = 100;

        array = new Integer[10];

        Integer i;

        for(i = 0; i < array.length; i++ ) {
            Integer randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
            array[i] = randomNum;
            System.out.println(array[i]);

        }

        Integer index = Arrays.asList(array).indexOf(getLowestValue(array));    

        System.out.println();
        System.out.println(getLowestValue(array) + " is the lowest value and it's index is " + index);

    }

        public static Integer getLowestValue(Integer[] array) {
            int minValue = array[0];
            //start at first index and recursively compare values to find the minimum value.
            for (int i = 1; i < array.length; i++) {
                if (array[i] < minValue) {
                    minValue = array[i];
                }
            }
            return minValue;
        }


}