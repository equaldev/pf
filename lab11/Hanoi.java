import java.util.*;

public class Hanoi {
    
    public static void main(String[] args){
	
        //start with 3 disks
        int disks = 3;

        
        //declare an array of integers representing the state of each peg
        ArrayList<Integer>[] pegs = new ArrayList[3];
        pegs[0] = new ArrayList<Integer>();
        pegs[1] = new ArrayList<Integer>();
        pegs[2] = new ArrayList<Integer>();

        //add number of disks to first array to start the puzzle
        for(int i = 0; i < disks; i++) {		
            pegs[0].add(i + 1);
        }
        
        //pass through number of disks, where they came from, destination and the contents of the current pegs array
        shiftLump(pegs[0].size(), 1, 3, 2, pegs);
    }

    //Move multiple disks at once
    public static void shiftLump(int disks, int from, int to, int using, ArrayList<Integer>[] pegs){

	//if there are no disks to move, finish
	if (disks == 0) {
	    return;
	}
	
	//move n-1 disks to the intermediate peg
	shiftLump(disks-1, from, using, to, pegs);
	
	//move nth disk to target peg
	shiftOne(pegs[from-1], pegs[to-1]);
		
	//print current state
	printPegs(pegs);
	
	//move n-1 disks to final peg
	shiftLump(disks-1, using, to, from, pegs);
    }
    
    public static void shiftOne(ArrayList<Integer> fromlist, ArrayList<Integer> tolist) {
	//remove top peg from fromlist
	int elt = fromlist.remove(0);
	//add top peg to top of tolist
	tolist.add(0, elt);

    }

    public static void printPegs(ArrayList<Integer>[] pegs) {
	//print contents of first peg 

	System.out.print("[ ");
	for(int i = 0; i < pegs[0].size(); i++) {

	    System.out.print(pegs[0].get(i));
	}
	System.out.print(" ]  ");
	
	//print contents of second peg
	System.out.print("[ ");
	for(int i = 0; i < pegs[1].size(); i++) {
	    System.out.print(pegs[1].get(i));
	}
	System.out.print(" ]  ");
	
	//print contents of third peg
	System.out.print("[ ");
	for(int i = 0; i < pegs[2].size(); i++) {
	    System.out.print(pegs[2].get(i));
	}
	System.out.println(" ]  ");
    }



    
}