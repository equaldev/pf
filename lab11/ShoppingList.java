import java.util.*;

class ShoppingList {
    

    public static void main (String[] args) {
        ArrayList<String> shopping = new ArrayList<String>();
        Scanner input = new Scanner(System.in);
        Boolean finished = false;

        do {
        System.out.println("Enter some stuff:");

            String item = input.next();

            if(item.equals("None"))  {
                finished = true;
            } 

            else {
                shopping.add(item);
            }

        } while (!finished);
        
        System.out.println();

        System.out.println("Shopping List:");
        System.out.println("----------------------------");
        
        for(int i = 0; i < shopping.size(); i++ ) {

            System.out.println(shopping.get(i));

        }

    }
 
}