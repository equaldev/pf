class TimeTable2DArray {

public static void main(String[] args) {

        int[][] numbers = new int[10][10];

        //set two arrays, one to hold rows. and one to hold col values
        numbers = multiply(9,9);


        for (int row = 0; row < numbers.length ; row++) {
                if (row == 0) {    
                    //bit lazy here           
                    System.out.println("* | 1  2  3  4  5  6  7  8  9");
                    System.out.print("-----------------------------");
                    System.out.println();
                
                }
            //For each row, loop through col value
            for (int col = 0; col < numbers[row].length; col++) {

                if (col == 0) {      
                    //row heading, using col value for row index         
                    System.out.print(numbers[row][col] +" |");
                
                }

                //Print rows and cols with two digits and a whitespace
                System.out.printf("%2d ",numbers[row][col]);
            
            }


            System.out.println();

        }
}

        public static int[][] multiply(int r, int c) {
            //store the multiplication values ina  2d array
            int [][] multiplication = new int[r][c];
            
            for (int row = 0; row < multiplication.length ; row++) {

                for (int col = 0; col < multiplication[row].length; col++) {

                    multiplication[row][col] = (row + 1) * ( col + 1);
                }

            }
            return multiplication;
        }

}
