import java.util.Scanner; 
import java.lang.Math;

class CartesianCentre{
	public static void main(String[] args) {
		//Load in Scanner
		Scanner scan = new Scanner(System.in);
		//Create New line variable
		String newLine = System.lineSeparator(); 

		//Constants
	//-----------------------------------------------------

		//Input Variables
	//-----------------------------------------------------

		//Start collecting input
		System.out.println("- Cartesian Centre - " + newLine);
		System.out.println("Enter Radius and Angle ");

		//Get user input for Radius
		System.out.print("Radius: ");
		double radius = scan.nextDouble();
				

		//Get user input for Angle
		System.out.print("Angle: ");
		double angle = scan.nextDouble();

        //Calculation
    //-------------------------------------------------------
        //x = r × cos φ
        //y = r × sin φ

     double x = radius * Math.cos(angle);
     double y = radius * Math.sin(angle);



		//Output
	//-------------------------------------------------------	
		
		//display results. 	
		System.out.print(newLine);
		System.out.println("Values:" + newLine);
		System.out.println("x = " + x);
		System.out.println("y = " + y);
        
        
	}

}