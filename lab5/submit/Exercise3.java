import java.util.Scanner; 
import java.lang.Math; 
import java.text.DecimalFormat;
import java.text.NumberFormat;


class Exercise3 {
    
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
        NumberFormat formatter = new DecimalFormat("#0.0");

        double nBLimit = 199.0;

		System.out.print("Enter floating point number: ");

		double nA = scan.nextDouble();

		double nB = Math.pow(nA,3.5);

        if (nB < nBLimit){

       		formatter.format(nB);
		    System.out.println(nB);

        } else {

		    System.out.println(nBLimit);

        }

	}

}