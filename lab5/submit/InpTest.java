import java.util.Scanner;
class InpTest {
    public static void main( String[] args )
    {
    Scanner keyboard = new Scanner(System.in);

    System.out.print("Enter a double: ");
    double db = keyboard.nextDouble();
    double db1 = keyboard.nextDouble();
    System.out.println("Input was: " + db + " " + db1);

    System.out.print("Enter an integer: ");
    int ia = keyboard.nextInt();
    System.out.println("Input was: " + ia);

    //This program wont work because the method nextDouble() saves the 
    //first double in the argument, and is not listening for the second argument. 
    //The fix involves adding another variable listening for the next input.
    }
}