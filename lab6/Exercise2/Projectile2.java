import java.util.Scanner;
import java.lang.Math;
class Trajectory {
    private static final double G = 9.8;
    public static double hMax;

// hMax = vo2/(2*G) 
public static double calculateMaxHeight(double args){
    double hMax = Math.pow(args,2)/(2*G);

    return hMax;

    //The difference from Exercise 1 is that this time
    // the method calculateMaxHeight is called from
    //the class Trajectory, not inside the main() function.
}

}
class Projectile {
    private static double maxHeight;
    public static double initalVelocity;
    public static void main(String[] args) {
        System.out.println("Enter an initial velocity:");

        Scanner keyboard = new Scanner(System.in);

        double initialVelocity = keyboard.nextDouble();

        Trajectory t = new Trajectory();

        maxHeight = t.calculateMaxHeight(initialVelocity);

        System.out.println(maxHeight);

    }

}