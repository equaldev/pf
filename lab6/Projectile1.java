import java.util.Scanner;
import java.lang.Math;

class Projectile {

    private static final double G = 9.8;
    private static double maxHeight;
    public static double initalVelocity;
    public static double hMax;

    public static void main(String[] args) {
        System.out.println("Enter an initial velocity:");

        Scanner keyboard = new Scanner(System.in);

        double initialVelocity = keyboard.nextDouble();

        maxHeight = calculateMaxHeight(initialVelocity);

        System.out.println(maxHeight);

    }

}