
/*
-name: String
-price: double
-scanCode: int
-numObjects: int = 0
+Product(String, double)
+getName(): String
+getScanCode: int
+getPrice(): double
+changePrice(double): void
+getNumObjects(): int
+getNumberInStock(): int
*/

class Product {
    private String name;
    private double price;
    private static int numObjects = 0;
    private int scanCode;

    public Product(String s, double d) {
        name = s;
        price = d;
        numObjects = numObjects + 1;
        scanCode = 1000 + numObjects;
    }

    public String getName() {
        return name;
    }

    public int getScanCode() {
        return scanCode;
    }

    public double getPrice() {
        return price;
    }

    public void changePrice(Double newPrice) {
        price = newPrice;

    }


    public static int getNumberInStock() {
        return numObjects;
    }
}



class TestProduct {
 public static void main(String[] args) {
 System.out.println("There are " + Product.getNumberInStock() + " items");

Product pr1 = new Product("Computer", 1500.0);
 System.out.println("There are " + Product.getNumberInStock() + " items");
Product pr2 = new Product("Printer", 600.0);
 Product pr3 = new Product("Monitor", 240.0);
System.out.println("There are " + Product.getNumberInStock() + " items");
 pr2.changePrice( 550.0 );
System.out.println(" -- Product info -- ");
System.out.println( "Name: " + pr2.getName() );
 System.out.println("Scan code: " + pr2.getScanCode() );
System.out.println("Price: " + pr2.getPrice() );
 }
}