class Circle {

    private static double r;
    public static void main(String[] args) {
    }
    public Circle( double r )
    {
        this.r = r;
    }
    public Circle(){

    };
    public void setRadius(double r){
        this.r = r;
    }

    public double getRadius(){
        return r;
    };

    public double calCircumference()
    {
    return 2*Math.PI*r;
    }
    public static double calArea()
    {
    return Math.PI*r*r;
}
    
}

class CircleApp
{
public static void main( String[] args ) {
    double rd = Double.parseDouble( args[0] );

    System.out.println( "Circle radius = " + rd );
    // create an object of Circle with the radius rd
    Circle circle1 = new Circle( rd );
    Circle circle2 = circle1;



    double cir2 = circle2.calCircumference();
    double area2 = circle2.calArea();

    double cir = circle1.calCircumference();
    double area = circle1.calArea();
    

    System.out.println("Circumference = " + cir);
    System.out.println("Area = " + area);
        System.out.println("Changes for 1 & 2:");

    System.out.println(" ");

    System.out.println("Circumference for Circle 2= " + cir2);
    System.out.println("Area for Circle 2 = " + area2);

    circle1.setRadius(4.3); //set radius for circle1
    System.out.println(" ");

    System.out.println("Changes for 1 & 2:");
    System.out.println(" ");
 
    System.out.println(" Circle 1= " + cir);
    System.out.println("Circle 2 = " + cir2); 

    Circle circle3 = circle2;  //set circle 3 as circle2
       System.out.println(" ");

    System.out.println("Changes for 2 & 3:");
    System.out.println(" ");
 
    System.out.println(" Circle 2 radius= " + circle2.getRadius());
    System.out.println("Circle 3 radius = " + circle3.getRadius()); 

        System.out.println(" ");

      System.out.println("Changes for 3:");
 
    System.out.println(" Circle 2 radius= " + circle2.getRadius());
    circle3.setRadius(7.8);
    System.out.println("Circle 3 radius = " + circle3.getRadius());     

}
}