/*
====================
Cash Register
--------------------
FIELDS
- items: int
- total: double
--------------------
METHODS
+ cashRegister()
+ scan(double): void
	or
+ scan(Product): void
+ getItems(): int
+ getTotalPrice(): double
--------------------*/
class CashRegister {

	private int items;
	private double total;


	public CashRegister(){

	};

	public void scan(Product product){
        items = items + 1;
        total = product.getPrice() + total;
	};

	public int getItems(){
        return items;

	};
	
	public double getTotal(){
		return total;
	};

}
/*
OUTPUT
***** At Register 1 *****
You have purchased 3 items
Total: $10.23
You have purchased 4 items
Total: $15.72

***** At Register 2 *****
You have purchased 2 items
Total: $4.74
*/

class CheckOut {
    public static void main(String[] args) {
        
        CashRegister register1 = new CashRegister();
        CashRegister register2 = new CashRegister();

        Product product1 = new Product("Milk", 8.2);
        Product product2 = new Product("Stuff", 3.9);
        Product product3 = new Product("Bread", 1.23);
        Product product4 = new Product("Apples", 13.52);


		System.out.println("***** At Register 1 *****");
        register1.scan(product1);
        register1.scan(product2);
        register1.scan(product3);
		System.out.println("You have purchased " + register1.getItems() + " items");
		System.out.println("Total: $" + register1.getTotal());

        register1.scan(product4);
		System.out.println("You have purchased " + register1.getItems() + " items");
		System.out.println("Total: $" + register1.getTotal());
		System.out.println(" ");

		System.out.println("***** At Register 2 *****");
        register2.scan(product4);
        register2.scan(product2);
		System.out.println("You have purchased " + register2.getItems() + " items");
		System.out.println("Total: $" + register2.getTotal());



        
    }
}

class Product {
	private String name;
	private int productID;
	private double price;
	private static int total;

	public Product(String productName, double productPrice) {
			name = productName;
			price = productPrice;
			total = total + 1;
			productID = total;
		}

	public String getName(){
		return name;
	};

	public int getProductID(){
		return productID;
	};

	public double getPrice(){
		return price;
	};

	public void changePrice(double newPrice){
		price = newPrice;
	};

	public static int getTotal(){
		return total;
	};



}