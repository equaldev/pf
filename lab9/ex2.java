import java.util.Scanner; 
import java.lang.Math;

class CheckPassFail{
/* 
 Write a program called CheckPassFail which prints "PASS" if the int variable
"mark" is more than or equal to 50; or prints "FAIL" otherwise. The program shall always
print “DONE” before exiting (Use IF…ELSE statement). 
*/
public static int mark;
	public static void main(String[] args) {
	  
    Scanner scan = new Scanner(System.in);

		System.out.println("Enter Grade: ");

     mark = scan.nextInt();
        
    if (mark >= 50) {
      System.out.println("PASS");
    }
    else {
      System.out.println("FAIL");
    }

    System.out.println("Done.");


       
	}

}
