import java.util.Scanner; 
import java.lang.Math;

class PrintNumberInWord{
/* 
Exercise 3: Write a program called PrintNumberInWord which prints "ONE", "TWO",... ,
"NINE", "OTHER" if the int variable "number" is 1, 2,... , 9, or other, respectively (Use a
"nested-if...else" statement). 
*/
 private static int number;
	public static void main(String[] args) {
	  
    Scanner scan = new Scanner(System.in);

	System.out.println("Enter Number: ");

    number = scan.nextInt();
        
    if (number > 0 && number < 2)
            System.out.println("ONE");
    if (number > 1 && number < 3)
            System.out.println("TWO");
    if (number > 2 && number < 4)
            System.out.println("THREE");  
    if (number > 3 && number < 5)
            System.out.println("FOUR");
    if (number > 4 && number < 6)
            System.out.println("FIVE");
    if (number > 5 && number < 7)
            System.out.println("SIX");
    if (number > 6 && number < 8)
            System.out.println("SEVEN");
    if (number > 7 && number < 9)
            System.out.println("EIGHT");
    if (number > 8 && number < 10)
            System.out.println("NINE");

    else if (number > 9) 
            System.out.println("OTHER");





  

       
	}

}
